defmodule TradeMasterWeb.PageLive do
  use TradeMasterWeb, :live_view

  @impl true
  def mount(_params, _session, socket) do
    {:ok, assign(socket, msg: "", messages: [])}
  end

  @impl true
  def handle_event("new-task", %{"m" => msg}, socket) do
    cond do
      String.length(msg) > 0 ->
        {:noreply, assign(socket, messages: [msg | socket.assigns.messages])}

      msg ->
        # TODO: push error
        # TODO: Delegate form validation to FE

        {:noreply, socket}
    end
  end

  @impl true
  def handle_event("remove-task", event, socket) do
    IO.puts("Remove Task")
    IO.inspect(event)
    IO.inspect(socket)

    {:noreply, socket}
  end
end
