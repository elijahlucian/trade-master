defmodule TradeMaster.Repo do
  use Ecto.Repo,
    otp_app: :trade_master,
    adapter: Ecto.Adapters.Postgres
end
