# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :trade_master,
  ecto_repos: [TradeMaster.Repo]

# Configures the endpoint
config :trade_master, TradeMasterWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "a2iyf0Hm8r0wAsvo3SOOMESuxOagkHAOZ5j5quDgTkaSEm0w+Hg6BxX2hRHTEhGf",
  render_errors: [view: TradeMasterWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: TradeMaster.PubSub,
  live_view: [signing_salt: "wd08Z/Gn"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
